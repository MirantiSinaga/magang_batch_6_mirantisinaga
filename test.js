const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://katamunda.cloud.javan.co.id/
  await page.goto('https://katamunda.cloud.javan.co.id/');

  // Click text=Tutup
  await page.click('text=Tutup');

  // Click #main >> text=Informasi
  await page.click('#main >> text=Informasi');

  // Click #main >> text=Transaksi
  await page.click('#main >> text=Transaksi');
  // assert.equal(page.url(), 'https://katamunda.cloud.javan.co.id/data-transaksi');

  // ---------------------
  await context.close();
  await browser.close();
})();